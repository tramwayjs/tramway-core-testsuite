const assert = require('assert');

/**
 * Returns the elements in the first array without those in the second.
 * 
 * @param {[]} a
 * @param {[]} b
 * @returns {[]}
 */
function getArrayDifference(a, b) {
    return a.filter(function(i) {return b.indexOf(i) < 0;})
}

/**
 * Returns the elements that are only common in both arrays.
 * 
 * @param {[]} a
 * @param {[]} b
 * @returns {[]}
 */
function getDeepArrayDifference(a, b) {
    return getArrayDifference(a, b)
        .concat(getArrayDifference(b, a))
        .filter(function(item, index, inputArray ) {
            return inputArray.indexOf(item) == index;
        });
}

/**
 * Extracts the names of arguments from a function and returns an array of strings.
 *
 * @param {Function} func
 * @return {Array.<String>}
 */
function parseFunction(func) {
    var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var ARGUMENT_NAMES = /([^\s,]+)/g;
    var fnStr = func.toString().replace(STRIP_COMMENTS, '');
    var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);

    if (result === null)
        result = [];

    return result;
}

/**
 * Gets functions from an object instance's prototype as an array.
 * 
 * @param {any} obj
 * @returns
 */
function getFunctions(obj) {
    return Object.getOwnPropertyNames(Object.getPrototypeOf(obj)).filter(function(prop) {
        return prop !== "constructor";
    });
}

/**
 * Gets Class's static functions as an array.
 * 
 * @param {any} classObj
 * @returns
 */
function getStaticFunctions(classObj) {
    return Object.getOwnPropertyNames(classObj).filter(function(prop){
        return typeof classObj[prop] === "function";
    });
}

/**
 * A set of tests to determine if any changes were made to a class.
 * 
 * @param {Object} libClass
 * @param {string} expectedClass
 * @param {string[]} expectedStaticPros
 * @param {string[]} expectedNonStaticProps
 * @param {function(Object, Object, string[], string[])} next Takes a chain function and passes the libClass, instance, expectedStaticProps, and expectedNonStaticProps
 * @returns
 */
function describeCoreClass(libClass, expectedClass, expectedStaticProps, expectedNonStaticProps, next) {
    return function() {
        var instance = new libClass();

        const CLASS_FUNCTIONS = getStaticFunctions(libClass);
        const INSTANCE_FUNCTIONS = getFunctions(instance);

        it("Should return a '" + expectedClass + "' class at the corresponding key", function(){
            assert.strictEqual(libClass.name, expectedClass);
        });

        it("Should not add new static functions. If so please update tests and version.", function(){
            assert.deepEqual(getArrayDifference(CLASS_FUNCTIONS, expectedStaticProps), []);
        });

        it("Should not remove any static functions. If so please update tests and version.", function(){
             assert.deepEqual(getArrayDifference(expectedStaticProps, CLASS_FUNCTIONS), []);
        });

        it("Should not add new instance functions. If so please update tests and version.", function(){
            assert.deepEqual(getArrayDifference(INSTANCE_FUNCTIONS, expectedNonStaticProps), []);
        });

        it("Should not remove any instance functions. If so please update tests and version.", function(){
             assert.deepEqual(getArrayDifference(expectedNonStaticProps, INSTANCE_FUNCTIONS), []);
        });

        if ("function" === typeof next) {
            return next(libClass, instance, expectedStaticProps, expectedNonStaticProps);
        }
    }
}

/**
 * A set of tests to determine if any changes were made to a function signature.
 * 
 * @param {function} func
 * @param {string[]} expectedArguments
 * @param {function} next
 * @returns
 */
function describeFunction(func, expectedArguments, next) {
    return function(){
        const funcArgs = parseFunction(func);

        it("Should not add new function parameters. If so please update tests and version.", function(){
            assert.deepEqual(getArrayDifference(funcArgs, expectedArguments), []);
        });

        it("Should not remove any function parameters. If so please update tests and version.", function(){
             assert.deepEqual(getArrayDifference(expectedArguments, funcArgs), []);
        });

        if ("function" === typeof next) {
            return next();
        }
    }
}

module.exports = {
    "getArrayDifference": getArrayDifference,
    "getDeepArrayDifference": getDeepArrayDifference,
    "parseFunction": parseFunction,
    "getFunctions": getFunctions,
    "getStaticFunctions": getStaticFunctions,
    "describeCoreClass": describeCoreClass,
    "describeFunction": describeFunction
};