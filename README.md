Tramway Test Suite is a simple library with functions meant for speeding up various tests using the Mocha testing framework.

The goal is that these tests break any time a core component changes in any way to act as a guidence toward version numbering and ensure that versions remain consistent for clients that use them. For the moment it doesn't go in depth.

# Installation:
1. `npm install tramway-core-testsuite --save-dev`

# Documentation
The test suite comes with the following methods:

| Method | Arguments | Return | Comments |
| --- | --- | --- | --- |
| getArrayDifference | array, array | array | Returns an array with items of first array without those in the second |
| getDeepArrayDifference | array, array | array | Runs getArrayDifference to both arrays and merges the result |
| parseFunction | function | array | Returns an array of string-represented arguments in the function signature |
| getFunctions | Object | array | Returns an array of string-represented functions in the instance's prototype and excludes the constructor |
| getStaticFunctions | Object | array | Returns an array of string-represented static functions from the Class |
| describeCoreClass | libClass: Class, expectedClass: string, expectedStaticProps: string[], expectedNonStaticProps: string[], next: function | function | A callback with 4 pre-made test cases to check if the class has changed in terms of its promises |
| describeFunction | func: Function, expectedArguments: string[], next: function | function | A callback with 2 pre-made test cases to check if the function has changed in terms of its promises |

# Usage

## Simple Usage
The `describeCoreClass` utility can just check if functions were added or removed and that the instances check up to expectations.

Here is an example of how you would import and use the library. For the most part, the describeCoreClass is sufficient. It will test the following criteria:
1. The expected class will be returned at the corresponding key in the library.
2. The expected class will not add new static functions.
3. The expected class will not remove any static functions.
4. The expected class will not add new instance functions.
5. The expected class will not remove any instance functions.

If tests #2 - #5 fail, it means that either a minor or major release respectively is merited.

Below you see how the Core's `Router` class can be tested for static and instance level function changes.

```
var utils = require('tramway-core-testsuite');

 describe("Should return a proper 'Router' class", utils.describeCoreClass(
    lib.Router, 
    "Router", 
    ["buildPath", "buildQuery"],
    ["initialize", "prepareRoute", "useMethod", "preparePath", "prepareArguments"]
));
```

## Thorough Usage
The `describeCoreClass` can have a function chained to it where the `describeFunction` utility can check the arguments of functions at the class and instance level passed conveniently from the `describeCoreClass` function.

Here is an example of something more thorough: you can chain a function-based test. In addition to the simple tests above, it will test the following criteria:
1. The expected function will not add new arguments.
2. The expected function will not remove existing arguments.

If either of these tests fail, it means that either a minor or major release respectively is merited.

Below you see how the Core's `Entity` class can be tested for method and argument changes.

```
var utils = require('tramway-core-testsuite');

describe("Should return a proper 'Entity' class", utils.describeCoreClass(
    lib.Entity, 
    "Entity", 
    [],
    ["hasAttribute", "serialize", "unserialize"],
    function(testClass, testInstance, classFunctions, instanceFunctions) {
        utils.describe("The 'hasAttribute' function should have the same signature", describeFunction(
            testInstance["hasAttribute"], 
            ["attribute"]
        ));
        describe("The 'serialize' function should have the same signature", describeFunction(
            testInstance["serialize"],
            []
        ));
        describe("The 'unserialize' function should have the same signature", describeFunction(
            testInstance["unserialize"],
            ["item"]
        ));
    }
));
```